# UFG-RDM-SummerSchool

## Description
The collection of open educational resources (OER) in this repository was created for the first Summer School on reserach data management for students of [Pre- and Protohistoric Archaeology of Kiel University](https://www.ufg.uni-kiel.de/en?set_language=en), conducted 2023-07-10 to 2023-07-14. The collection contains presentations given during the Summer School and expanded collaboratively by teachers and students during the Summer School.

* Introduction to Researach Data Management (RDM)
* Researach Data Management as part of project planning 
* Publication of data in archaeological data bases
* Practicing RDM pipeline

The individual elements can be reused to create informational and training materials on research data management.

## Authors
Prof. Dr. Oliver Nakoinz, Dr. Lizzie Scholtus, Hartwig Bünning, Britta Petersen, Julian Schönebaum, Daniela Engelhart, Eric Pallmer 

## License 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) unless otherwise stated

## Cite the collection as
**Enter Zenodo Citation here...**

## How to handle
Markdown files provided here prpared to be used in [LiaScript](https://liascript.github.io/). In order to use the files in LiaScript go to https://liascript.github.io/ and insert the link to the text box "Enter your course url ...". Then hit the button "Load course!"

The material is also available on Zenodo **[Enter Link to Publication here]** 

## File structure
Folders an files in this repository are organsied the following way

* 
